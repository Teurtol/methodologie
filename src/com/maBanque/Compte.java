package com.maBanque;

public interface Compte {

	/** 
	*	Credite le compte du montant donne en argument. 
	*	@param credit 
	*	@throws Exception si credit <= 0 
	*/ 
	 	void crediter(float credit) throws Exception; 
	 
	 	float getSolde(); 
	 
	/** 
	*	Debite le compte de la valeur donnee en argument 
	*	@param debit 
	*	@return le montant demande si le solde du compte est superieur au montant demande, 
	*	retourne la valeur du solde si le solde est inferieur au montant demande.  
	*	@throws Exception si debit < 20 ou si debit > 1000 euros. 
	*/ 
	 	float debiter(float debit) throws Exception; 
	 	 
	/** 
	*	Reinitialise 
	*	@param solde 
	*	@throws Exception si solde <= 0 
	*/ 
	 	void setSolde(float solde) throws Exception; 
	 

}
